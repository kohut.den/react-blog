export const FAQData = [{
  id: 1,
  title: 'How to use Context API?',
  content: "Answer: Commodo Leo Vivamus Euismod Urna Elit at,, Tellus Ornare Euismod Semper Non At morbi, Eget Mauris Finibus Laoreet Volutpat Iaculis Magna mattis. Ultricies Dictum Pretium Aliquam Metus ac, Eget Turpis arcu"
}, {
  id: 2,
  title: 'Pros and Cons of Context API',
  content: "Answer: Nisl Fermentum Lectus Mauris Convallis Ut aliquam. Consectetur maximus; Convallis Massa Ipsum Semper Vel Ipsum a. Lacus Elit Mi Enim Praesent massa? Morbi Dictumst Amet Eleifend Morbi urna? Orci Odio iaculis!"
}, {
  id: 3,
  title: 'SSR with React, how to do it without pain',
  content: "Answer: Leo Fermentum Dui Lorem Quam Eu nunc,; Eu Orci Vel Cursus Urna sit? Odio Posuere Odio Enim Sit accumsan"
}, {
  id: 4,
  title: 'Can I use Class based components',
  content: "Answer: Sapien Consequat Ante Tellus eget; In iaculis. Amet Amet Odio Ipsum Justo varius! Lectus Ut Augue Amet tristique?"
}]
