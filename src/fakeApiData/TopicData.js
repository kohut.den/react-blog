export const TopicData = [{
  id: 1,
  title: 'Advanced Patterns',
  content: "Author: Kitze Talk & Workshop"
}, {
  id: 2,
  title: 'Coding an SVG with Reactjs',
  content: "Author: Elizabet Oliveira"
}, {
  id: 3,
  title: 'State Management with React Hooks',
  content: "Author: State Management with React Hooks"
}, {
  id: 4,
  title: 'Building a design system',
  content: "Author: Sid"
}, {
  id: 5,
  title: 'Microfrontends',
  content: "Author: David Den Toom"
}]
