export const ShowForm = ({ onSubmit, titleLabel, contentLabel }) => (
  <form className="flex-column form" onSubmit={onSubmit}>
    <label htmlFor="title">{titleLabel}:</label>
    <input type="text" name="title"/>
    <label htmlFor="title">{contentLabel}:</label>
    <textarea name="content"/>
    <button >Add {titleLabel}</button>
  </form>
)
