export const ShowButton = ({ toggleForm, titleLabel }) => (
  <button onClick={toggleForm} className="form-button"> Create a new {titleLabel}</button>
)
