import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { ShowForm, ShowButton } from './atoms';

export const Form = ({ items, details, updateHandler }) => {
  const [isVisible, setIsVisible] = useState(false);
  const { titleLabel, contentLabel } = details;

  const toggleForm = () => {
    setIsVisible(!isVisible);
  }

  const onSubmit = (event) => {
    event.preventDefault()
    const { title, content } = event.target;

    if (!title.value || !content.value) {
      return;
    }

    updateHandler([
      ...items, {
        id: uuidv4(),
        title: title.value,
        content: `${contentLabel}: ${content.value}`
      }
    ]);

    title.value = '';
    content.value = ''
    toggleForm()
  }

  return (
    isVisible
      ? <ShowForm
          onSubmit={onSubmit}
          titleLabel={titleLabel}
          contentLabel={contentLabel}
        />
      : <ShowButton
          toggleForm={toggleForm}
          titleLabel={titleLabel}
        />
  );
}

