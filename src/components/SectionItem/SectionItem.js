export const SectionItem = ({ title, content }) => {
  return (
    <li>
      <h3>{title}</h3>
      {content}
    </li>
  );
}

