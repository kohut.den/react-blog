import { SectionItem } from '../SectionItem';

export const Section = ({title, items = [], className}) => {
  return (
    <section className={className}>
      <h2>{title} ({items.length})</h2>
      <ul>
        {items.map(({ id, title, content }) => (
          <SectionItem key={id} title={title} content={content} />
        ))}
      </ul>
    </section>
  );
}

