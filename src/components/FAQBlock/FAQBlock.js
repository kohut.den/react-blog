import { memo } from 'react';
import { Section } from "../Section";
import { Form } from "../Form";

const FAQBlockComponent = ({ faq, setFaq }) => {
  console.log('render FAQ');

  const formDetails = {
    titleLabel: 'Question',
    contentLabel: 'Answer',
  }

  return (
    <div>
      <Section title="FAQ:" items={faq} className="Blog-questions"/>
      <Form items={faq} details={formDetails} updateHandler={setFaq} />
    </div>
  );
}

export const FAQBlock = memo(FAQBlockComponent);

