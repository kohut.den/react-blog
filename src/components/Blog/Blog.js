import { useState } from 'react';

import { FAQBlock } from '../FAQBlock';
import { TopicsBlock } from '../TopicsBlock';

import { FAQData } from '../../fakeApiData/FAQData';
import { TopicData } from "../../fakeApiData/TopicData";
import logo from '../../logo.svg';

import './Blog.css';

export const Blog = () => {
  const [topics, setTopic] = useState(TopicData)
  const [faq, setFaq] = useState(FAQData)

  return (
    <div className="Blog">
      <header className="Blog-header">
          <h1>React Blog</h1>
          <img src={logo} className="Blog-logo" alt="logo" />
      </header>
      <main>
        <TopicsBlock topics={topics} setTopic={setTopic} />
        <FAQBlock faq={faq} setFaq={setFaq} />
      </main>
    </div>
  );
}
