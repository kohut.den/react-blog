import { Section } from '../Section';
import { Form } from '../Form';

export const TopicsBlock = ({ topics, setTopic }) => {
  console.log('render Topics');

  const formDetails = {
    titleLabel: 'Topic',
    contentLabel: 'Author',
  }

  return (
    <div>
      <Section title="Topics:" items={topics} />
      <Form items={topics} details={formDetails} updateHandler={setTopic} />
    </div>
  );
}

